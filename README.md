
# WiFi-AP

A very simple, Debian-based WiFi Access Point that just bridges to an ethernet network.


## System requirements

* Debian 11 (bullseye)
* ssh to root possible
* i386/amd64/ARM

Here is an example hardware setup:
* https://wiki.debian.org/InstallingDebianOn/Allwinner#Installing_from_an_SD_card_image

WiFi AP
* https://wiki.archlinux.org/title/Software_access_point#Wi-Fi_link_layer
* build driver for Edimax AC600 USB
  https://github.com/aircrack-ng/rtl8812au


Plan B: RPi
* https://di-marco.net/blog/it/2020-04-11-ap_setup_and_wifi_management_for_debian_devices/


## Deployment to remote server

You simply need to run the playbook from this project to a remote
host.  First you'll need to install ansible, on your local machine
e.g.:

    sudo apt-get install ansible

You'll also need to make sure that the bare minimum for ansible
is installed on your remote host:

    sudo apt-get install python3 sudo

Then you can immediately start the deployment:

    ansible-playbook -i remoteHost, provision.yml

